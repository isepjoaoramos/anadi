# Packages usados 
library(readxl)
library(tidyverse)
library(reshape2)
library(dplyr)
library(stats)
library(nortest)
library(car)
library(dunn.test)
library(FSA)
library(data.table)

# Importação de dados do ficheiro excel 
# Utilizar o path onde está localizado o ficheiro
covid_data <- read_excel("Desktop/Semestre/ANADI/owid-covid-data.xlsx")


## 4.1 - Análise de Dados

########################## 4.1 A) ############################

# Filtragem dos dados por continente
covid_data.filtrados_a <- filter(covid_data,iso_code == "OWID_NAM" | iso_code == "OWID_AFR" | 
                        iso_code == "OWID_SAM"| iso_code == "OWID_ASI" | iso_code == "OWID_OCE"| iso_code == "OWID_EUR")

# Criação de um gráfico que tem no eixo do y os casos totais 
# e no eixo do x a data
# Objetivo de mostrar evolução do número total de infetados ao longo do tempo por continente
ggplot(data = covid_data.filtrados_a) + 
  geom_point(mapping = aes(x = date, y = total_cases, color = location))



########################## 4.1 B) ############################

#Filtragem dos dados por continente
covid_data.filtrados_d <- filter(covid_data,iso_code=="OWID_NAM" | iso_code=="OWID_AFR" | iso_code=="OWID_SAM"| iso_code=="OWID_ASI" | iso_code=="OWID_OCE"| iso_code=="OWID_EUR")

#Retirar as linhas que apresentam NA no campo total_cases_per_million
dados_b_semNA <- covid_data.filtrados_d[!is.na(covid_data.filtrados_d$total_cases_per_million),]

#Desenhar o gr�fico resultante (y=total_cases_per_million,x= date)
ggplot(data = dados_b_semNA) + 
  geom_point(mapping = aes(x = date, y = total_cases_per_million, color=location))


########################## 4.1 C) ############################

# Objetivo: boxplot do nº de mortos diários p/ milhão para Portugal, Espanha, Itália e Reino Unido (sem outliers).

# Filtragem dos dados de Portugal
covid_data.portugal <- filter(covid_data,location=="Portugal")
covid_data.portugalNA <- filter(covid_data.portugal,new_deaths_per_million != "NA")
Q1.portugal=quantile(covid_data.portugalNA$new_deaths_per_million,0.25)
Q3.portugal=quantile(covid_data.portugalNA$new_deaths_per_million,0.75)
AIQ.portugal=(Q3.portugal-Q1.portugal) 
limEsquerda.portugal=Q1.portugal-(1.5*AIQ.portugal)
limDireita.portugal=Q3.portugal+(1.5*AIQ.portugal)
covid_data.portugalSemOutliers <- filter(covid_data.portugalNA,new_deaths_per_million < limDireita.portugal,new_deaths_per_million > limEsquerda.portugal)

# Filtragem dos dados de Espanha
covid_data.espanha <- filter(covid_data, location=='Spain')
covid_data.espanhaNA <- filter(covid_data.espanha, new_deaths_per_million != "NA")
Q1.espanha=quantile(covid_data.espanhaNA$new_deaths_per_million,0.25)
Q3.espanha=quantile(covid_data.espanhaNA$new_deaths_per_million,0.75)
AIQ.espanha=(Q3.espanha-Q1.espanha)
limEsquerda.espanha=Q1.espanha-(1.5*AIQ.espanha)
limDireita.espanha=Q3.espanha+(1.5*AIQ.espanha)
covid_data.espanhaSemOutliers <- filter(covid_data.espanhaNA,new_deaths_per_million<limDireita.espanha, new_deaths_per_million>limEsquerda.espanha)

# Filtragem dos dados de Italia
covid_data.italia <- filter(covid_data, location=='Italy')
covid_data.italiaNA <- filter(covid_data.italia, total_deaths_per_million != "NA")
Q1.italia=quantile(covid_data.italiaNA$new_deaths_per_million,0.25)
Q3.italia=quantile(covid_data.italiaNA$new_deaths_per_million,0.75)
AIQ.italia=(Q3.italia-Q1.italia)
limEsquerda.italia=Q1.italia-(1.5*AIQ.italia)
limDireita.italia=Q3.italia+(1.5*AIQ.italia)
covid_data.italiaSemOutliers <- filter(covid_data.italiaNA,new_deaths_per_million<limDireita.italia,new_deaths_per_million>limEsquerda.italia)

# Filtragem dos dados de Reino Unido
covid_data.reinoUnido <- filter(covid_data, location=='United Kingdom')
covid_data.reinoUnidoNA <- filter(covid_data.reinoUnido, total_deaths_per_million != "NA")
Q1.reinoUnido=quantile(covid_data.reinoUnidoNA$new_deaths_per_million,0.25)
Q3.reinoUnido=quantile(covid_data.reinoUnidoNA$new_deaths_per_million,0.75)
AIQ.reinoUnido=(Q3.reinoUnido-Q1.reinoUnido)
limEsquerda.reinoUnido=Q1.reinoUnido-(1.5*AIQ.reinoUnido)
limDireita.reinoUnido=Q3.reinoUnido+(1.5*AIQ.reinoUnido)
covid_data.reinoUnidoSemOutliers <- filter(covid_data.reinoUnidoNA,new_deaths_per_million<limDireita.reinoUnido,new_deaths_per_million>limEsquerda.reinoUnido)

# Criacao do boxplot
classes=c("Portugal","Espanha","Italia","Reino Unido")
boxplot(covid_data.portugalSemOutliers$new_deaths_per_million, covid_data.espanhaSemOutliers$new_deaths_per_million, covid_data.italiaSemOutliers$new_deaths_per_million, covid_data.reinoUnidoSemOutliers$new_deaths_per_million, names=classes, main='Mortos diarios por milhao de habitantes')


########################## 4.1 D) ############################

# Filtragem dos dados (Ir filtrar apenas os dados referentes a Alemanha, Albania, Dinamarca, Rússia)
covid_data.filtrados_d <- filter(covid_data,iso_code == "DEU" | iso_code == "ALB"| iso_code == "DNK" | iso_code == "RUS")


# Utilização da função "melt" para criar um dataframe contendo apenas as colunas "location" "total_deaths_per_milion" e "total_tests_per_thousand"
covid_data.filtrados_d_melted <- melt(covid_data.filtrados_d[c('location', 'total_deaths_per_million','total_tests_per_thousand')])

# Criação de um gráfico de barras com a informação pedida
ggplot(covid_data.filtrados_d_melted,aes(location, value, fill = variable))+
  geom_bar(stat="identity",position="dodge")



########################## 4.1 E) ############################

#Filtragem dos dados para obter apenas os pa�ses europeus
covid_data.europa <- filter(covid_data,continent=="Europe")

#Obter os dados sem NA no campo new_cases_per_million
dados_e_semNA <- covid_data.europa[!is.na(covid_data.europa$new_cases_per_million),]

#Ordenar os dados para obter o maior n�mero de new_cases_per_million no topo
ordered <- dados_e_semNA[order(-dados_e_semNA$new_cases_per_million),]

#Obter o pa�s com maior n�mero de new_cases_per_million
ordered[1,"location"]


########################## 4.1 F) ############################

# Ordenar covid_data pelo campo reproduction rate
ordered <- covid_data[order(-covid_data$reproduction_rate),]

# Extracao de dados sobre a ocorrencia em que $reproduction_rate foi maior
pais=ordered[1,"location"]
dia=ordered[1,"date"]

# Conversao para texto
diaString=paste(unlist(dia), collapse='')
paisString=paste(unlist(pais),collapse='')

# Impressao do resultado na consola
cat("R: Registou-se a maior taxa de transmissibilidade do virus no dia ",diaString," no/a ", paisString)


########################## 4.1 G) ############################

# Manipulação de Dados
covid_data.daily_deaths_africa <- filter(covid_data,iso_code == "OWID_AFR")
covid_data.daily_deaths_europe <- filter(covid_data, iso_code == "OWID_EUR")
covid_data.daily_deaths_north_america <- filter(covid_data,iso_code == "OWID_NAM")
covid_data.daily_deaths_south_america <- filter(covid_data,iso_code == "OWID_SAM")
covid_data.daily_deaths_asia <- filter(covid_data,iso_code == "OWID_ASI")
covid_data.daily_deaths_oceania <- filter(covid_data,iso_code == "OWID_OCE")

# Remover NAs
covid_data.daily_deaths_south_america[15][is.na(covid_data.daily_deaths_south_america[15])] <- 0


# Remover Outliers

#Africa
Q1.africa=quantile(covid_data.daily_deaths_africa$new_deaths_per_million,0.25)
Q3.africa=quantile(covid_data.daily_deaths_africa$new_deaths_per_million,0.75)
AIQ.africa=(Q3.africa-Q1.africa)
limEsquerda.africa=Q1.africa-(1.5*AIQ.africa)
limDireita.africa=Q3.africa+(1.5*AIQ.africa)
covid_data.daily_deaths_africa_NoOutliers <- filter(covid_data.daily_deaths_africa,new_deaths_per_million<limDireita.africa, new_deaths_per_million>limEsquerda.africa)

#Europe
Q1.europe=quantile(covid_data.daily_deaths_europe$new_deaths_per_million,0.25)
Q3.europe=quantile(covid_data.daily_deaths_europe$new_deaths_per_million,0.75)
AIQ.europe=(Q3.europe-Q1.europe)
limEsquerda.europe=Q1.europe-(1.5*AIQ.europe)
limDireita.europe=Q3.europe+(1.5*AIQ.europe)
covid_data.daily_deaths_europe_NoOutliers <- filter(covid_data.daily_deaths_europe,new_deaths_per_million<limDireita.europe, new_deaths_per_million>limEsquerda.europe)


#North America
Q1.north_america=quantile(covid_data.daily_deaths_north_america$new_deaths_per_million,0.25)
Q3.north_america=quantile(covid_data.daily_deaths_north_america$new_deaths_per_million,0.75)
AIQ.north_america=(Q3.north_america-Q1.north_america)
limEsquerda.north_america=Q1.north_america-(1.5*AIQ.north_america)
limDireita.north_america=Q3.north_america+(1.5*AIQ.north_america)
covid_data.daily_deaths_north_america_NoOutliers <- filter(covid_data.daily_deaths_north_america,new_deaths_per_million<limDireita.north_america, new_deaths_per_million>limEsquerda.north_america)



# South America
Q1.south_america=quantile(covid_data.daily_deaths_south_america$new_deaths_per_million,0.25)
Q3.south_america=quantile(covid_data.daily_deaths_south_america$new_deaths_per_million,0.75)
AIQ.south_america=(Q3.south_america-Q1.south_america)
limEsquerda.south_america=Q1.south_america-(1.5*AIQ.south_america)
limDireita.south_america=Q3.south_america+(1.5*AIQ.south_america)
covid_data.daily_deaths_south_america_NoOutliers <- filter(covid_data.daily_deaths_south_america,new_deaths_per_million<limDireita.south_america, new_deaths_per_million>limEsquerda.south_america)


#Asia
Q1.asia=quantile(covid_data.daily_deaths_asia$new_deaths_per_million,0.25)
Q3.asia=quantile(covid_data.daily_deaths_asia$new_deaths_per_million,0.75)
AIQ.asia=(Q3.asia-Q1.asia)
limEsquerda.asia=Q1.asia-(1.5*AIQ.asia)
limDireita.asia=Q3.asia+(1.5*AIQ.asia)
covid_data.daily_deaths_asia_NoOutliers <- filter(covid_data.daily_deaths_asia,new_deaths_per_million<limDireita.asia, new_deaths_per_million>limEsquerda.asia)


#Oceania
Q1.oceania=quantile(covid_data.daily_deaths_oceania$new_deaths_per_million,0.25)
Q3.oceania=quantile(covid_data.daily_deaths_oceania$new_deaths_per_million,0.75)
AIQ.oceania=(Q3.oceania-Q1.oceania)
limEsquerda.oceania=Q1.oceania-(1.5*AIQ.oceania)
limDireita.oceania=Q3.oceania+(1.5*AIQ.oceania)
covid_data.daily_deaths_oceania_NoOutliers <- filter(covid_data.daily_deaths_oceania,new_deaths_per_million<limDireita.oceania, new_deaths_per_million>limEsquerda.oceania)


classes=c("Africa","Europe","NA","SA","Asia", "Oceania")
boxplot(covid_data.daily_deaths_africa_NoOutliers$new_deaths_per_million, 
        covid_data.daily_deaths_europe_NoOutliers$new_deaths_per_million, 
        covid_data.daily_deaths_north_america_NoOutliers$new_deaths_per_million, 
        covid_data.daily_deaths_south_america_NoOutliers$new_deaths_per_million, 
        covid_data.daily_deaths_asia_NoOutliers$new_deaths_per_million,
        covid_data.daily_deaths_oceania_NoOutliers$new_deaths_per_million,
        names=classes, main='Mortos diarios por milhao de habitantes por continente')





# 4.2 - Inferência Estatística


########################## 4.2 A) ############################

covid_data.data_uk <- filter(covid_data, iso_code == "GBR", date >= "2020-04-01", date <= "2021-02-27")
covid_data.data_portugal <- filter(covid_data, iso_code == "PRT", date >= "2020-04-01", date <= "2021-02-27")

set.seed(118)

random_uk <- sample_n(covid_data.data_uk, 30)

set.seed(118)

random_portugal <- sample_n(covid_data.data_portugal, 30)

# Extrai coluna do rt (reproduction_rate)
rt_portugal <- select(random_portugal, reproduction_rate)
rt_uk <- select(random_uk, reproduction_rate)

# Converter dataframes em vetores para usar na função t.test
vector_rt_portugal <- as.vector(rt_portugal[['reproduction_rate']])
vector_rt_uk <- as.vector(rt_uk[['reproduction_rate']])

# Verificação se dados tem distribuição normal
# Utilização do teste Lilliefors pois trata-se de uma amostra com 30 ou mais elementos
lillie.test(vector_rt_portugal) # p-value: 0,2495
lillie.test(vector_rt_uk) #p-value: 0,8732

# Como o p-value nos dois casos (vector_rt_portugal e vector_rt_uk) é superior
# a 0,05, então podemos concluir que os dados têm distribuição normal, então podemos
# usar testes paramétricos

test <- t.test(vector_rt_uk, vector_rt_portugal,alternative = ("greater"), paired = FALSE)
test$p.value
## p-value = 0.80


# Ou seja, como o p-value é superior a 0,05, então significa que a evidência estatística 
# não nos permite rejeitar a hipótese nula, isto é que o rt do reino unido é inferior ou igual
# ao rt de portugal.



########################## 4.2 B) ############################

#Obter apenas os dados de Portugal com data ap�s 2020-04-01
covid_data.portugal <- filter(covid_data,location=="Portugal",date >= "2020-04-01")

#Definir a seed 115 (para obter os resultados de form constante) e obter 15 dias aleat�rios da amostra anterior
set.seed(115); dados_15dias_portugal <- covid_data.portugal[sample(1:nrow(covid_data.portugal),15),];

#Obten��o dos dias aleat�rios  
random_days <- dados_15dias_portugal$date

#Filtra��o dos dados para Fran�a e obten��o dos dados de Fran�a com os dias aleat�rios
covid_data.france <- filter(covid_data,location=="France")
dados_15dias_france <- covid_data.france[covid_data.france$date %in% random_days,];

#Filtra��o dos dados para a Alemanha e obten��o dos dados da Alemanha com os dias aleat�rio
covid_data.germany <- filter(covid_data,location=="Germany")
dados_15dias_germany <- covid_data.germany[covid_data.germany$date %in% random_days,];

#Filtra��o dos dados para Espanha e obten��o dos dados de Espanha com os dias aleat�rio
covid_data.spain <- filter(covid_data,location=="Spain")
dados_15dias_spain <- covid_data.spain[covid_data.spain$date %in% random_days,];

#Ordena��o dos dados por data
sample_portugal <- dados_15dias_portugal[order(dados_15dias_portugal$date),]
sample_spain <- dados_15dias_spain[order(dados_15dias_spain$date),]
sample_france <- dados_15dias_france[order(dados_15dias_france$date),]
sample_germany <- dados_15dias_germany[order(dados_15dias_germany$date),]

#Obten��o do campo new_deaths_per_million para cada pa�s
deaths_germany <- sample_germany$new_deaths_per_million
deaths_portugal <- sample_portugal$new_deaths_per_million
deaths_france <- sample_france$new_deaths_per_million
deaths_spain <- sample_spain$new_deaths_per_million

#Cria��o de uma estrutura composta pelos valores anteriores
data_2b <- c(deaths_portugal,deaths_spain,deaths_france,deaths_germany)

#Teste de Shapiro para verificar a normalidade em cada pa�s (n<30)
shapiro.test(deaths_germany)

#Shapiro-Wilk normality test
#
#data:  deaths_germany
#W = 0.49776, p-value = 3.374e-06

shapiro.test(deaths_france)

#Shapiro-Wilk normality test
#
#data:  deaths_france
#W = 0.67432, p-value = 0.0001338

shapiro.test(deaths_spain)

#Shapiro-Wilk normality test
#
#data:  deaths_spain
#W = 0.76977, p-value = 0.001544

shapiro.test(deaths_portugal)

#Shapiro-Wilk normality test
#
#data:  deaths_portugal
#W = 0.488, p-value = 2.819e-06


# As populações NÃO são normais (p-value<0.05 em todos os casos), por isso não é possível efetuar um t-test.

#H0 -> Os grupos não apresentam diferenças significativas
#H1 -> Os grupos apresentam diferenças significativas

#Agrupamento dos dados
grupos<-factor(c(rep("portugal",15),rep("spain",15),rep("france",15),rep("germany",15)))

#Teste de Levene para verificar a homogeneidade da vari�ncia
leveneTest(data_2b,grupos)

#Levene's Test for Homogeneity of Variance (center = median)
#      Df F value Pr(>F)
#group  3  0.5827 0.6288
#      56 

#p-value > 0.05, por isso as variâncias são semelhantes

#Teste de Kruskal para verificar se existem diferen�as significativas entre os grupos
kruskal.test(data_2b ~ grupos)

#Kruskal-Wallis rank sum test
#
#data:  data_2b by grupos
#Kruskal-Wallis chi-squared = 7.4407, df = 3, p-value = 0.0591

#p-value > 0.05, por isso não se rejeita a hipótese nula, ou seja, não existem diferenças significativas no nº de mortes,
#por milhão de habitantes, em Espanha, França, Portugal e Itália


########################## 4.2 C) ############################


# Dados filtrados por continente e data
dados.africa <- filter(covid_data,location=="Africa",date>="2020-04-01")
dados.asia <- filter(covid_data,location=="Asia",date>="2020-04-01")
dados.europa <- filter(covid_data,location=="Europe",date>="2020-04-01")
dados.americaNorte <- filter(covid_data,location=="North America",date>="2020-04-01")
dados.americaSul <- filter(covid_data,location=="South America",date>="2020-04-01")

# Extracao de 30 ocorrencias aleatorias de cada populacao
set.seed(100); random_africa <- dados.africa[sample(1:nrow(dados.africa),30),];
set.seed(101); random_asia <- dados.asia[sample(1:nrow(dados.asia),30),];
set.seed(102); random_europa <- dados.europa[sample(1:nrow(dados.europa),30),];
set.seed(103); random_americaNorte <- dados.americaNorte[sample(1:nrow(dados.americaNorte),30),];
set.seed(104); random_americaSul <- dados.americaSul[sample(1:nrow(dados.americaSul),30),];

# Vetor de new_deaths_per_million
mortosDiariosPorMilhao.africa <- c(random_africa$new_deaths_per_million)
mortosDiariosPorMilhao.asia <- c(random_asia$new_deaths_per_million)
mortosDiariosPorMilhao.europa <- c(random_europa$new_deaths_per_million)
mortosDiariosPorMilhao.americaNorte <- c(random_americaNorte$new_deaths_per_million)
mortosDiariosPorMilhao.americaSul <- c(random_americaSul$new_deaths_per_million)

# Teste de Lilliefors
lillie.test(mortosDiariosPorMilhao.africa); # p-value = 0.0024 (<0.05)
lillie.test(mortosDiariosPorMilhao.europa); # p-value = .000000418 (< 0.05)
lillie.test(mortosDiariosPorMilhao.americaNorte); # p-value = 0.000743 (< 0.05)

lillie.test(mortosDiariosPorMilhao.asia); # p-value = 0.2816 (> 0.05)
lillie.test(mortosDiariosPorMilhao.americaSul); # p-value = 0.4198 (> 0.05)

# R: As amostras da Asia e America do Sul aparentam seguir uma distribuicao normal, na medida
# em que as restantes, com um p-value < 0.05, nao. 

# Teste de Kruskal-Wallis
dadosNumericos <- c(mortosDiariosPorMilhao.africa,mortosDiariosPorMilhao.asia,mortosDiariosPorMilhao.europa,mortosDiariosPorMilhao.americaNorte,mortosDiariosPorMilhao.americaSul)
grupos <- factor(c(rep("Africa",length(mortosDiariosPorMilhao.africa)),rep("Asia",length(mortosDiariosPorMilhao.asia)),rep("Europa",length(mortosDiariosPorMilhao.europa)),rep("NA",length(mortosDiariosPorMilhao.americaNorte)),rep("SA",length(mortosDiariosPorMilhao.americaSul))))
kruskal.test(dadosNumericos ~ grupos)
# Kruskal-Wallis chi-squared = 107.45, df = 4, p-value < 2.2e-16
# R: O p-value leva-nos a nao rejeitar H0. Os grupos apresentam a mesma distribuicao. 

# Analise post hoc:
dunnTest(dadosNumericos ~ grupos)

#Comparison          P.adj
#1    Africa - Asia  8.213275e-01 (> 0.05)
#2  Africa - Europa  3.637144e-06 (< 0.05)
#3    Asia - Europa  9.518063e-06 (< 0.05)
#4      Africa - NA  1.189892e-13 (< 0.05)
#5        Asia - NA  6.138441e-13 (< 0.05)
#6      Europa - NA  2.549681e-02 (< 0.05)
#7      Africa - SA  2.974811e-11 (< 0.05)
#8        Asia - SA  1.257758e-10 (< 0.05)
#9      Europa - SA  1.506013e-01 (> 0.05)
#10         NA - SA  8.830367e-01 (> 0.05)


# 4.3 - Correlação

########################## 4.3 A) ############################

#Filtragem dos dados por data e popula��o
covid_data.3a <- filter(covid_data,continent=="Europe", date >= "2021-01-01", population > 10000000)

#Data frame dos dados com os campos location, reporduction_rate e poulation_density 
dados_filtrados_3a <- data.frame(location = covid_data.3a$location, reproduction_rate = covid_data.3a$reproduction_rate, population = covid_data.3a$population_density)

#Obten��o das linhas com a m�xima taxa de transmissibildade por cada pa�s europeu com mais de 10M habitantes
final_data <- setDT(dados_filtrados_3a)[ , .SD[which.max(reproduction_rate)], by = location]   # Apply data.table functions

#Teste de Pearson para verificar a correla��o entre os dados
cor.test(final_data$reproduction_rate,final_data$population,method="pearson",alternative="greater")


#Pearson's product-moment correlation
#
#data:  final_data$reproduction_rate and final_data$population
#t = -0.64421, df = 13, p-value = 0.7347
#alternative hypothesis: true correlation is greater than 0
#95 percent confidence interval:
# -0.5733925  1.0000000
#sample estimates:
#      cor 
#-0.175886 


# A associação entre variáveis é baixa (r=-0.18) e o coeficiente de correlação não é significativo (p-value>0.05).

########################## 4.3 B) ############################

# Dados crus
paisesEuroGrandes <- filter(covid_data,total_deaths!="NA",continent=="Europe", date >= "2021-01-01" ,population>10000000)

# Criacao do data frame
dataFrame <- data.frame(location = paisesEuroGrandes$location, total_deaths = paisesEuroGrandes$total_deaths_per_million, elderlyPercent = paisesEuroGrandes$aged_65_older)

# Identificacao dos arrays a passar ao teste de correlacao linear de Pearson
x=c(dataFrame$total_deaths)
y=c(dataFrame$elderlyPercent)

# Execucao do teste
cor.test(x,y,alternative="greater",method="pearson")

# Output:
# Pearson's product-moment correlation

# data:  x and y
# t = 9.9727, df = 868, p-value < 2.2e-16
# alternative hypothesis: true correlation is greater than 0
# 95 percent confidence interval:
#  0.2696454 1.0000000
# sample estimates:
#      cor 
# 0.320625 

# R.: A medida de associação entre as variáveis é baixa (r= 0.32), logo as variaveis estao fracamente correlacionadas.
# O coeficiente de correlacao e significativo (p-value = 2.2*10^-16 < 0.05).
# Coeficiente de determinacao r^2 = 0,1028


# 4.4 Regressão 

########################## 4.4 A) ############################


# Manipulação de dados - Criação de dataframes para a info de cada mês
covid_data.april <- filter(covid_data, iso_code == "PRT", date >= "2020-04-01", date <= "2020-04-30")
covid_data.may <- filter(covid_data, iso_code == "PRT", date >= "2020-05-01", date <= "2020-05-31")
covid_data.june <- filter(covid_data, iso_code == "PRT", date >= "2020-06-01", date <= "2020-06-30")
covid_data.july <- filter(covid_data, iso_code == "PRT", date >= "2020-07-01", date <= "2020-07-31")
covid_data.august <- filter(covid_data, iso_code == "PRT", date >= "2020-08-01", date <= "2020-08-30")
covid_data.september <- filter(covid_data, iso_code == "PRT", date >= "2020-09-01", date <= "2020-09-31")
covid_data.october <- filter(covid_data, iso_code == "PRT", date >= "2020-10-01", date <= "2020-10-31")
covid_data.november <- filter(covid_data, iso_code == "PRT", date >= "2020-11-01", date <= "2020-11-30")
covid_data.december <- filter(covid_data, iso_code == "PRT", date >= "2020-12-01", date <= "2020-12-31")
covid_data.january <- filter(covid_data, iso_code == "PRT", date >= "2021-01-01", date <= "2021-01-31")
covid_data.february <- filter(covid_data, iso_code == "PRT", date >= "2021-02-01", date <= "2021-02-27")


# Média de Si (Stringency Index)
average_si_april <- mean(covid_data.april$stringency_index)
average_si_may <- mean(covid_data.may$stringency_index)
average_si_june <- mean(covid_data.june$stringency_index)
average_si_july <- mean(covid_data.july$stringency_index)
average_si_august <- mean(covid_data.august$stringency_index)
average_si_september <- mean(covid_data.september$stringency_index)
average_si_october <- mean(covid_data.october$stringency_index)
average_si_november <- mean(covid_data.november$stringency_index)
average_si_december <- mean(covid_data.december$stringency_index)
average_si_january <- mean(covid_data.january$stringency_index)
average_si_february <- mean(covid_data.february$stringency_index)

# Média de dr (death rate - média de mortes diárias por milhão de habitantes)
average_dr_april <- mean(covid_data.april$new_deaths_per_million)
average_dr_may <- mean(covid_data.may$new_deaths_per_million)
average_dr_june <- mean(covid_data.june$new_deaths_per_million)
average_dr_july <- mean(covid_data.july$new_deaths_per_million)
average_dr_august <- mean(covid_data.august$new_deaths_per_million)
average_dr_september <- mean(covid_data.september$new_deaths_per_million)
average_dr_october <- mean(covid_data.october$new_deaths_per_million)
average_dr_november <- mean(covid_data.november$new_deaths_per_million)
average_dr_december <- mean(covid_data.december$new_deaths_per_million)
average_dr_january <- mean(covid_data.january$new_deaths_per_million)
average_dr_february <- mean(covid_data.february$new_deaths_per_million)

# Média de dc (daily cases - média de casos diários por milhão de habitantes)
average_dc_april <- mean(covid_data.april$new_cases_per_million)
average_dc_may <- mean(covid_data.may$new_cases_per_million)
average_dc_june <- mean(covid_data.june$new_cases_per_million)
average_dc_july <- mean(covid_data.july$new_cases_per_million)
average_dc_august <- mean(covid_data.august$new_cases_per_million)
average_dc_september <- mean(covid_data.september$new_cases_per_million)
average_dc_october <- mean(covid_data.october$new_cases_per_million)
average_dc_november <- mean(covid_data.november$new_cases_per_million)
average_dc_december <- mean(covid_data.december$new_cases_per_million)
average_dc_january <- mean(covid_data.january$new_cases_per_million)
average_dc_february <- mean(covid_data.february$new_cases_per_million)

# Média de rr (reproduction rate  - média de taxa de transmissibilidade)
average_rr_april <- mean(covid_data.april$reproduction_rate)
average_rr_may <- mean(covid_data.may$reproduction_rate)
average_rr_june <- mean(covid_data.june$reproduction_rate)
average_rr_july <- mean(covid_data.july$reproduction_rate)
average_rr_august <- mean(covid_data.august$reproduction_rate)
average_rr_september <- mean(covid_data.september$reproduction_rate)
average_rr_october <- mean(covid_data.october$reproduction_rate)
average_rr_november <- mean(covid_data.november$reproduction_rate)
average_rr_december <- mean(covid_data.december$reproduction_rate)
average_rr_january <- mean(covid_data.january$reproduction_rate)
average_rr_february <- mean((covid_data.february[!is.na(covid_data.february$reproduction_rate),])$reproduction_rate)

average_si <- c(average_si_april, average_si_may, average_si_june, average_si_july,average_si_august, average_si_september, average_si_october, average_si_november, average_si_december, average_si_january, average_si_february)
average_dr <- c(average_dr_april, average_dr_may, average_dr_june, average_dr_july, average_dr_august, average_dr_september, average_dr_october, average_dr_november, average_dr_december, average_dr_january, average_dr_february)
average_dc <- c(average_dc_april, average_dc_may, average_dc_june, average_dc_july, average_dc_august, average_dc_september, average_dc_october, average_dc_november, average_dc_december, average_dc_january, average_dc_february)
average_rr <- c(average_rr_april, average_rr_may, average_rr_june, average_rr_july, average_rr_august, average_rr_september, average_rr_october, average_rr_november, average_rr_december, average_rr_january, average_rr_february)


data_regression.model <- data.frame(average_si, average_dc, average_rr, average_dr)

# Construção de modelo de regressão múltipla 
regression_model <- lm(average_si~average_dr+average_dc+average_rr, data = data_regression.model)

#Info sobre o modelo + grafico
summary(regression_model)
par ( mfrow =c(2,2))
plot(regression_model)


########################## 4.4 B) ############################

#4.4 b)

# Homocedasticidade

#Constru��o do gr�fico com a vari�vel resltado e os res�duos
plot ( fitted ( regression_model) , residuals ( regression_model), xlab =" Val . Ajust .", ylab =" Res .")
abline ( h=0)

# Os dados est?o espalhados pelo gr?fico, por isso os res?duos, em princ�pio, s?o homoced?sticos (no entanto, como temos poucos dados, ser?o comparadas as vari?veis resultado e independentes)

#Para cada vari�vel independente, os dados s�o divididos pela mediana das vari�veis; � verificada a igualdade da vari�ncia no res�duos para cada uma das vari�veis

mx1=median(average_dr);mx1
var.test(residuals(regression_model)[average_dr>mx1],residuals(regression_model)[average_dr<mx1])

#F test to compare two variances
#
#data:  residuals(regression_model)[average_dr > mx1] and residuals(regression_model)[average_dr < mx1]
#F = 8.9936, num df = 3, denom df = 4, p-value = 0.05975
#alternative hypothesis: true ratio of variances is not equal to 1
#95 percent confidence interval:
#  0.9012389 135.8128045
#sample estimates:
#  ratio of variances 
#8.993642 

mx1=median(average_dc);mx1
var.test(residuals(regression_model)[average_dc>mx1],residuals(regression_model)[average_dc<mx1])

#F test to compare two variances
#
#data:  residuals(regression_model)[average_dc > mx1] and residuals(regression_model)[average_dc < mx1]
#F = 2.7744, num df = 3, denom df = 4, p-value = 0.3493
#alternative hypothesis: true ratio of variances is not equal to 1
#95 percent confidence interval:
#  0.2780215 41.8966348
#sample estimates:
#  ratio of variances 
#2.774432 

mx1=median(average_rr);mx1
var.test(residuals(regression_model)[average_rr>mx1],residuals(regression_model)[average_rr<mx1])

#F test to compare two variances
#
#data:  residuals(regression_model)[average_rr > mx1] and residuals(regression_model)[average_rr < mx1]
#F = 0.41606, num df = 4, denom df = 3, p-value = 0.4162
#alternative hypothesis: true ratio of variances is not equal to 1
#95 percent confidence interval:
#  0.02755199 4.15196524
#sample estimates:
#  ratio of variances 
#0.416062 


# Os p-values s?o todos maiores que 0.05, por isso pode-se confirmar que as vari?ncias s?o iguais e existe homocedasticidade dos res?duos


# Autocorrela??o nula

#Teste de Durbin-Watson para verificar a independ�ncia dos res�duos
durbinWatsonTest(regression_model)

#lag Autocorrelation D-W Statistic p-value
#1     -0.04809952      1.510627   0.106
#Alternative hypothesis: rho != 0

# p-value > 0.05, por isso os res?duos s?o independentes



# Multicolinearidade

#Teste VIF para verificar a multicolinearidade
factor.de.inflacao.de.variancia<-vif(regression_model)
factor.de.inflacao.de.variancia


#average_dr average_dc average_rr 
#25.923674  28.425241   1.717774 

# Existe multicolinearidade (average_dr e average_dc > 3)


########################## 4.4 C) ############################

x0=data.frame(average_dr=10, average_dc=460, average_rr=1.1)
predict(regression_model,x0)

# Output:
# > predict(regression_model,x0)
# 1 
# 78.6761 

# R.: Para os valores Dm=10,Cm=460 e Rm=1.1, a estimativa de Ir é de 78.6761.








